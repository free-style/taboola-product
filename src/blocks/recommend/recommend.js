// Recommend block
((window, Taboola) => {
    Taboola.UI = Taboola.UI || {};

    class Recommend {
        constructor(config) {
            if (!config || typeof config !== "object") {
                Taboola.error("invalid block configuration");
            }
            this.config = config;
            this.layoutInstructions = {};
            this.intiThumbnailURL();
            this.initURL();
            this.initBranding();
            this.initOrigin();
            this.initTitle();
            this.initDescription();
            this.lazyLoadObserver();
        }

        intiThumbnailURL() {
            const thumbnailArr = this.config["thumbnail"];
            if (!Array.isArray(thumbnailArr)) {
                Taboola.error("thumbnail value should be an array");
            }
            if (thumbnailArr.length !== 1) {
                Taboola.error("thumbnail array should have one object");
            }
            const thumbnailObj = thumbnailArr[0];
            if (!thumbnailObj || typeof thumbnailObj !== "object") {
                Taboola.error("not valid thumbnail object");
            }
            const thumbnailURL = thumbnailObj["url"];
            if (typeof thumbnailURL !== "string" || thumbnailURL.indexOf("http") === -1) {
                Taboola.error("not valid thumbnail URL");
            }
            this.layoutInstructions.thumbnailURL = thumbnailURL;
        }

        initURL() {
            const url = this.config["url"];
            if (typeof url !== "string" || url.indexOf("http") === -1) {
                Taboola.error("not valid recommend URL");
            }
            this.layoutInstructions.url = url;
        }

        initOrigin() {
            const origin = this.config["origin"];
            if (origin !== "sponsored" && origin !== "organic") {
                Taboola.error("not valid origin type");
            }
            this.layoutInstructions.origin = origin;
        }

        initBranding() {
            let branding = this.config["branding"];
            if (typeof branding !== "string") {
                branding = "";
            }
            this.layoutInstructions.branding = branding;
        }

        initTitle() {
            const title = this.config["name"];
            if (typeof title !== "string") {
                Taboola.error("not valid recommend name");
            }
            this.layoutInstructions.title = title;
        }

        initDescription() {
            let description = this.config["description"];
            if (typeof description !== "string") {
                description = "";
            }
            this.layoutInstructions.description = description;
        }

        getOriginEl() {
            const originEl = document.createElement("div");
            originEl.setAttribute("class", "taboola-recommend-origin");
            const span1 = document.createElement("span");
            span1.innerText = this.layoutInstructions.branding;
            const span2 = document.createElement("span");
            span2.innerText = "|";
            const span3 = document.createElement("span");
            span3.innerText = this.layoutInstructions.origin;
            originEl.appendChild(span1);
            originEl.appendChild(span2);
            originEl.appendChild(span3);
            return originEl;
        }

        lazyLoadObserver() {
            const imgOptions = {
                threshold: 0.2,
            };
            const imgObserver = new IntersectionObserver((entries, imgObserver) => {
                entries.forEach((entry) => {
                    if (!entry.isIntersecting) return;
                    const img = entry.target;
                    img.src = this.layoutInstructions.thumbnailURL;
                    imgObserver.unobserve(entry.target);
                });
            }, imgOptions);
            this.imgObserver = imgObserver;
        }

        getAnchorEl() {
            const anchorEl = document.createElement("a");
            anchorEl.setAttribute("href", this.layoutInstructions.url);
            if (this.layoutInstructions.origin === "sponsored") {
                anchorEl.setAttribute("target", "blank");
            }

            var thumbnailImg = new Image();
            this.imgObserver.observe(thumbnailImg);
            thumbnailImg.dataset.src = this.layoutInstructions.thumbnailURL;
            thumbnailImg.setAttribute("class", "taboola-thumbnail-image");
            thumbnailImg.setAttribute("alt", this.layoutInstructions.title);

            const title = document.createElement("div");
            title.setAttribute("class", "taboola-title");
            title.innerText = this.layoutInstructions.title;

            const description = document.createElement("div");
            description.setAttribute("class", "taboola-description");
            description.innerText = this.layoutInstructions.description;

            anchorEl.appendChild(thumbnailImg);
            anchorEl.appendChild(title);
            anchorEl.appendChild(description);
            return anchorEl;
        }

        render = (el) => {
            if (!(el instanceof HTMLElement)) {
                Taboola.error("Please provide element to render this block inside it");
            }
            el.innerHTML = "";
            let anchorEl = this.getAnchorEl();
            if (this.layoutInstructions.origin === "sponsored") {
                anchorEl.appendChild(this.getOriginEl());
            }
            const recommendEl = document.createElement("div");
            recommendEl.setAttribute("class", "taboola-recommend");
            el.appendChild(recommendEl);
            recommendEl.appendChild(anchorEl);
        };
    }

    Taboola.UI.Recommend = Recommend;
})(window, (window.Taboola = window.Taboola || {}));
