// Taboola APi
((window, Taboola) => {
    Taboola.UI = Taboola.UI || {};

    class Recommendation {
        constructor(config) {
            if (!config || typeof config !== "object") {
                Taboola.error("invalid widget configuration");
            }
            this.config = config;
            this.publisherId = this.getPublisherId();
            this.requestParams = this.getRequestParams();
        }
        getRequestParams() {
            const paramsObj = this.config.params;
            if (!paramsObj || typeof paramsObj !== "object") {
                Taboola.error(`Please provide request params`);
            }
            const requestedParams = ["app.type", "app.apikey", "source.id"];
            requestedParams.forEach((paramKey) => {
                if (!Object.prototype.hasOwnProperty.call(paramsObj, paramKey)) {
                    Taboola.error(`Please provide ${paramKey} parameter`);
                }
            });
            return paramsObj;
        }

        getPublisherId() {
            const publisherId = this.config.publisherId;
            if (typeof publisherId !== "string" || publisherId.length === 0) {
                Taboola.error(`Please provide publisher id`);
            }
            return publisherId;
        }

        sendGetRecommendations() {
            const defaultURL = `http://api.taboola.com/1.0/json`;
            const URL = `${defaultURL}/${this.publisherId}/recommendations.get`;
            Taboola.utils.network.getAjax(URL, this.handleRecommendationsRes, this.requestParams);
        }

        handleRecommendationsRes = (response) => {
            let objResponse = null;
            try {
                objResponse = JSON.parse(response);
            } catch (e) {
                Taboola.error("recommendation widget cannot process response", e);
            }
            const layoutInstructions = objResponse;
            if (!Array.isArray(layoutInstructions["list"])) {
                Taboola.error("recommendation widget expect recommendation list of type array");
            }
            if (layoutInstructions["list"].length === 0) {
                Taboola.popup(this.widgetWrapper, "There is no recommendations for you right now, Try again later !");
            }
            this.render(layoutInstructions);
        };
        getWidgetContainer() {
            let widgetContainer = document.getElementById(this.config.widgetContainerId);
            if (!widgetContainer) {
                widgetContainer = document.body;
            }
            return widgetContainer;
        }
        start = () => {
            let widgetContainer = this.getWidgetContainer();
            widgetContainer.innerHTML = "";
            let widgetWrapper = document.createElement("div");
            widgetWrapper.setAttribute("class", "taboola-widget-wrapper");
            widgetContainer.appendChild(widgetWrapper);
            this.widgetWrapper = widgetWrapper;
            this.sendGetRecommendations();
        };
        render(layoutInstructions) {
            const recommendationsEl = document.createElement("div");
            recommendationsEl.setAttribute("class", "taboola-recommendation-widget");
            this.widgetWrapper.appendChild(recommendationsEl);
            layoutInstructions["list"].forEach((recommendConfig, index) => {
                const recommend = new Taboola.UI.Recommend(recommendConfig);
                const col = document.createElement("div");
                col.setAttribute("class", "taboola-col-" + index);
                recommend.render(col);
                recommendationsEl.appendChild(col);
            });
        }
    }

    Taboola.UI.Recommendation = Recommendation;
})(window, (window.Taboola = window.Taboola || {}));
