// Taboola APi
((window, Taboola) => {
    Taboola.UI = Taboola.UI || {};
    Taboola.UI.widgetFactory = new (class {
        constructor() {
            this.widgetFactoryMap = {
                recommendation: Taboola.UI.Recommendation,
            };
        }

        get widgets() {
            return this.widgetFactoryMap;
        }

        createWidget = (widgetType, config) => {
            const widgetFactoryMap = this.widgets;
            if (!Object.prototype.hasOwnProperty.call(widgetFactoryMap, widgetType)) {
                Taboola.error(`${widgetType} is not valid widget type.`);
            }
            const widgetClass = widgetFactoryMap[widgetType];
            return new widgetClass(config);
        };
    })();
})(window, (window.Taboola = window.Taboola || {}));
