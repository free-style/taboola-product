(() => {
    let config = {
        // widget container
        widgetContainerId: "taboola-recommendation-widget-container",
        // params needed for this test
        params: {
            "app.type": "desktop",
            "app.apikey": "f9040ab1b9c802857aa783c469d0e0ff7e7366e4",
            count: "50",
            "source.type": "video",
            "source.id": "214321562187",
            "source.url": "http://www.site.com/videos/214321562187.html",
        },
        publisherId: "taboola-templates",
        widgetType: "recommendation",
    };
    document.getElementById("start").addEventListener("click", (e) => {
        e.target.closest(".button-container").remove();
        taboola.startWidget(config);
    });
})();
