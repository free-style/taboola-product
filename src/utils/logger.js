// Taboola utils logger
((window, Taboola) => {
    Taboola.utils = Taboola.utils || {};
    Taboola.utils.logger = new (class {
        constructor() {}
        popup = (el, msg) => {
            const popupContainer = document.createElement("div");
            popupContainer.setAttribute("class", "taboola-popup");
            popupContainer.innerText = msg;
            el.appendChild(popupContainer);
        };
        log = (msg) => {
            console.log(msg);
        };
        /*
            This function should handle log errors
            msg: String represents message to log.
            error: error object to throw.
            throwError: boolean => true throw Error otherwise ignore
            logError: boolean => true console.error msg otherwise ignore.
         */
        error = (msg, errorObj, throwError = true) => {
            msg = typeof msg === "string" ? msg : "empty error message";
            errorObj = typeof errorObj !== "undefined" && errorObj instanceof Error ? errorObj : new Error(msg);
            if (throwError) {
                throw errorObj;
            }
        };
    })();
    Taboola.log = Taboola.utils.logger.log.bind(Taboola.utils.logger);
    Taboola.popup = Taboola.utils.logger.popup.bind(Taboola.utils.logger);
    Taboola.error = Taboola.utils.logger.error.bind(Taboola.utils.logger);
})(window, (window.Taboola = window.Taboola || {}));
