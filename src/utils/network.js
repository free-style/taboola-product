// Taboola utils network
((window, Taboola) => {
    "use-strict";
    Taboola.utils = Taboola.utils || {};
    Taboola.utils.network = new (function () {
        this.buildAjaxQuery = function (paramsObj) {
            let query = [];
            for (const key in paramsObj) {
                if (Object.prototype.hasOwnProperty.call(paramsObj, key)) {
                    query.push(encodeURIComponent(key) + "=" + encodeURIComponent(paramsObj[key]));
                }
            }
            return "?" + query.join("&");
        };
        /*
            This function should send an ajax request.
            URL: A String URL representing the URL to send the request to.
            paramsObj represent query params to add to URL.
         */
        this.getAjax = function (url, onSuccess, paramsObj, async = true, timeout = 3000) {
            const xhr = new XMLHttpRequest();
            onSuccess =
                typeof onSuccess === "function"
                    ? onSuccess
                    : (res) => {
                          console.log("requested succeeded - no function to handle it");
                      };
            xhr.onload = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    if (xhr.status === 200) {
                        onSuccess(xhr.responseText);
                    } else {
                        Taboola.error(xhr);
                    }
                }
            };
            url = url + this.buildAjaxQuery(paramsObj);
            xhr.open("GET", url, async);
            xhr.timeout = timeout;
            xhr.ontimeout = function (e) {
                console.error("The request for " + url + " timed out.");
            };
            xhr.send();
        };
    })();
})(window, (window.Taboola = window.Taboola || {}));
