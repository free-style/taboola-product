// Taboola APi
((window, Taboola) => {
    Taboola.API = Taboola.API || {};
    Taboola.API.Taboola = new (class {
        constructor() {}

        startWidget = (config) => {
            if (typeof config === "undefined" || config === null) {
                throw { message: "taboola.startWidget Error: invalid widget configuration object" };
            }
            const widgetType = config.widgetType;
            const widget = Taboola.UI.widgetFactory.createWidget(widgetType, config);
            widget.start();
        };
    })();
    window.taboola = Taboola.API.Taboola;
})(window, (window.Taboola = window.Taboola || {}));
