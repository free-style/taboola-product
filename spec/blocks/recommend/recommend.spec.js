describe("recommend.js", () => {
    const testConfig = {
        description: "Dans",
        type: "video",
        name: "Duo fraise-e",
        created: "Thu, 19 Apr 2018 12:22:00 UTC",
        branding: "Cuisine",
        duration: "0",
        views: "0",
        thumbnail: [
            {
                url: "https://images.taboola.come.jpg",
            },
        ],
        categories: ["fr"],
        id: "~~V1~~-8309618725535",
        origin: "sponsored",
        url: "https://api.tabB",
    };
    const blockClass = Taboola.UI.Recommend;
    let config, block;
    describe("recommend", () => {
        describe("constructor", () => {
            it("not valid config", () => {
                expect(() => {
                    new blockClass();
                }).toThrowError(`invalid block configuration`);
            });
            it(`initiating config, layoutInstructions`, () => {
                const spyIntiThumbnailURL = spyOn(blockClass.prototype, "intiThumbnailURL").and.callThrough();
                const spyInitURL = spyOn(blockClass.prototype, "initURL").and.callThrough();
                const spyInitBranding = spyOn(blockClass.prototype, "initBranding").and.callThrough();
                const spyInitOrigin = spyOn(blockClass.prototype, "initOrigin").and.callThrough();
                const spyInitTitle = spyOn(blockClass.prototype, "initTitle").and.callThrough();
                const spyInitDescription = spyOn(blockClass.prototype, "initDescription").and.callThrough();
                const spyLazyLoadObserver = spyOn(blockClass.prototype, "lazyLoadObserver").and.callThrough();
                let recommend = new blockClass(testConfig);
                let layoutInstructions = recommend.layoutInstructions;
                expect(recommend.config).toEqual(testConfig);
                expect(layoutInstructions.thumbnailURL).toEqual(testConfig.thumbnail[0].url);
                expect(layoutInstructions.origin).toEqual(testConfig.origin);
                expect(layoutInstructions.url).toEqual(testConfig.url);
                expect(layoutInstructions.branding).toEqual(testConfig.branding);
                expect(layoutInstructions.title).toEqual(testConfig.name);
                expect(layoutInstructions.description).toEqual(testConfig.description);
                expect(spyIntiThumbnailURL).toHaveBeenCalledTimes(1);
                expect(spyInitURL).toHaveBeenCalledTimes(1);
                expect(spyInitBranding).toHaveBeenCalledTimes(1);
                expect(spyInitOrigin).toHaveBeenCalledTimes(1);
                expect(spyInitTitle).toHaveBeenCalledTimes(1);
                expect(spyInitDescription).toHaveBeenCalledTimes(1);
                expect(spyLazyLoadObserver).toHaveBeenCalledTimes(1);
            });
        });
        describe("initThumbnailURL", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("thumbnail value is not array", () => {
                config.thumbnail = {};
                expect(() => {
                    block = new blockClass(config);
                    block.intiThumbnailURL();
                }).toThrowError("thumbnail value should be an array");
            });
            it("thumbnail value array with many objects", () => {
                config.thumbnail = [1, 2, 3];
                expect(() => {
                    block = new blockClass(config);
                    block.intiThumbnailURL();
                }).toThrowError("thumbnail array should have one object");
            });
            it("thumbnail value array at index 0 is not valid object", () => {
                config.thumbnail[0] = "sdgsd";
                expect(() => {
                    block = new blockClass(config);
                    block.intiThumbnailURL();
                }).toThrowError("not valid thumbnail object");
            });
            it("thumbnail URL not valid URL, should contain http", () => {
                config.thumbnail[0].url = "sdgsd";
                expect(() => {
                    block = new blockClass(config);
                    block.intiThumbnailURL();
                }).toThrowError("not valid thumbnail URL");
            });
            it("thumbnail URL is valid", () => {
                block = new blockClass(config);
                block.intiThumbnailURL();
                expect(block.layoutInstructions.thumbnailURL).toBe(config.thumbnail[0].url);
            });
        });
        describe("initURL", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("not valid URL, string without http", () => {
                expect(() => {
                    config.url = "fgsdg";
                    block = new blockClass(config);
                    block.initURL();
                }).toThrowError(`not valid recommend URL`);
            });
            it("not valid URL, it is not string", () => {
                expect(() => {
                    config.url = 12412;
                    block = new blockClass(config);
                    block.initURL();
                }).toThrowError(`not valid recommend URL`);
            });
            it("valid URL", () => {
                block = new blockClass(config);
                block.initURL();
                expect(block.layoutInstructions.url).toEqual(testConfig.url);
            });
        });
        describe("intOrigin", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("not valid origin, it is not 'sponsored' nor 'organic'", () => {
                expect(() => {
                    config.origin = "fgsdg";
                    block = new blockClass(config);
                    block.initOrigin();
                }).toThrowError("not valid origin type");
            });
            it("valid origin", () => {
                block = new blockClass(config);
                block.initOrigin();
                expect(block.layoutInstructions.origin).toBe(config.origin);
            });
        });
        describe("initBranding", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("branding not string", () => {
                config.branding = {};
                let block = new blockClass(config);
                block.initBranding();
                expect(block.layoutInstructions.branding).toBe("");
            });
            it("branding is string", () => {
                let block = new blockClass(config);
                block.initBranding();
                expect(block.layoutInstructions.branding).toBe(config.branding);
            });
        });
        describe("getTitle", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("recommend name is not string", () => {
                expect(() => {
                    config.name = [];
                    block = new blockClass(config);
                    block.initTitle();
                }).toThrowError(`not valid recommend name`);
            });
            it("valid recommend name", () => {
                block = new blockClass(config);
                block.initTitle();
                expect(block.layoutInstructions.title).toBe(testConfig.name);
            });
        });
        describe("initDescription", () => {
            let config;
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("getDescription not string", () => {
                config.description = {};
                block = new blockClass(config);
                block.initDescription();
                expect(block.layoutInstructions.description).toBe("");
            });
            it("getDescription is string", () => {
                block = new blockClass(config);
                block.initDescription();
                expect(block.layoutInstructions.description).toBe(config.description);
            });
        });
        describe("getOriginEl", () => {
            it("return origin container and 3 spans", () => {
                config = JSON.parse(JSON.stringify(testConfig));
                config.origin = "sponsored";
                block = new blockClass(config);
                const container = block.getOriginEl();
                const spans = container.children;
                expect(container.tagName).toBe("DIV");
                expect(container.classList.contains("taboola-recommend-origin")).toBeTruthy;
                expect(spans.length).toBe(3);
                expect(spans[0].innerText).toBe(config.branding);
                expect(spans[1].innerText).toBe("|");
                expect(spans[2].innerText).toBe("sponsored");
            });
        });
        describe("getAnchorEl", () => {
            beforeAll(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("Anchor El organic type", () => {
                config.origin = "organic";
                block = new blockClass(config);
                const anchorEl = block.getAnchorEl();
                expect(anchorEl.tagName).toBe("A");
                expect(anchorEl.getAttribute("href")).toBe(config.url);
                expect(anchorEl.getAttribute("target")).toBe(null);
                const img = anchorEl.querySelectorAll("img");
                expect(img.length).toBe(1);
                expect(img[0].dataset.src).toBe(config["thumbnail"][0].url);
                expect(img[0].classList.contains("taboola-thumbnail-image"));
                const title = anchorEl.querySelectorAll(".taboola-title");
                expect(title.length).toBe(1);
                expect(title[0].innerText).toBe(config.name);
                const description = anchorEl.querySelectorAll(".taboola-description");
                expect(description.length).toBe(1);
                expect(description[0].innerText).toBe(config.description);
            });
            it("Anchor El sponsored type", () => {
                config.origin = "sponsored";
                block = new blockClass(config);
                const anchorEl = block.getAnchorEl();
                expect(anchorEl.tagName).toBe("A");
                expect(anchorEl.getAttribute("href")).toBe(config.url);
                expect(anchorEl.getAttribute("target")).toBe("blank");
                const img = anchorEl.querySelectorAll("img");
                expect(img.length).toBe(1);
                expect(img[0].dataset.src).toBe(config["thumbnail"][0].url);
                expect(img[0].classList.contains("taboola-thumbnail-image"));
                const title = anchorEl.querySelectorAll(".taboola-title");
                expect(title.length).toBe(1);
                expect(title[0].innerText).toBe(config.name);
                const description = anchorEl.querySelectorAll(".taboola-description");
                expect(description.length).toBe(1);
                expect(description[0].innerText).toBe(config.description);
                expect(anchorEl.children[0]).toEqual(img[0]);
                expect(anchorEl.children[1]).toEqual(title[0]);
                expect(anchorEl.children[2]).toEqual(description[0]);
            });
        });
        describe("render", () => {
            let el, spyGetAnchorEl, spyGetOriginEl;
            beforeAll(() => {
                config = JSON.parse(JSON.stringify(testConfig));
                block = new blockClass(config);
                el = document.createElement("div");
            });
            beforeEach(() => {
                spyGetAnchorEl = spyOn(blockClass.prototype, "getAnchorEl").and.callThrough();
                spyGetOriginEl = spyOn(blockClass.prototype, "getOriginEl").and.callThrough();
            });
            it("render sponsored recommend inside provided element", () => {
                config.origin = "sponsored";
                block = new blockClass(config);
                block.render(el);
                expect(spyGetAnchorEl).toHaveBeenCalledTimes(1);
                expect(spyGetOriginEl).toHaveBeenCalledTimes(1);
                expect(el.children.length).toBe(1);
                expect(el.children[0].classList.contains("taboola-recommend")).toBeTruthy();
            });
            it("render organic recommend inside provided element", () => {
                config.origin = "organic";
                block = new blockClass(config);
                block.render(el);
                expect(spyGetAnchorEl).toHaveBeenCalledTimes(1);
                expect(spyGetOriginEl).toHaveBeenCalledTimes(0);
                expect(el.children.length).toBe(1);
                expect(el.children[0].classList.contains("taboola-recommend")).toBeTruthy();
            });
            it("render recommend without providing element instance of HTMLElement", () => {
                expect(() => {
                    block.render("what ever");
                }).toThrowError("Please provide element to render this block inside it");
            });
        });
    });
});
