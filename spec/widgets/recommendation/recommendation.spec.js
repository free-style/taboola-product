describe("recommendation.js", () => {
    let testConfig = {
        // widget container
        widgetContainerId: "taboola-recommendation-widget-container",
        // params needed for this test
        params: {
            "app.type": "desktop",
            "app.apikey": "f9040ab1b9c802857aa783c469d0e0ff7e7366e4",
            count: "50",
            "source.type": "video",
            "source.id": "214321562187",
            "source.url": "http://www.site.com/videos/214321562187.html",
        },
        publisherId: "taboola-templates",
        widgetType: "recommendation",
    };
    const widgetClass = Taboola.UI.Recommendation;
    describe("recommendation", () => {
        let widget, config, widgetConfig;
        describe("constructor", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("not valid config", () => {
                config = undefined;
                expect(() => {
                    widget = new widgetClass(config);
                }).toThrowError("invalid widget configuration");
            });
            it("initiating config, publisherId, requestParams", () => {
                widget = new widgetClass(config);
                expect(widget.config).toEqual(testConfig);
                expect(widget.requestParams).toEqual(testConfig.params);
                expect(widget.publisherId).toEqual(testConfig.publisherId);
            });
            it("constructor calls getRequestParams", () => {
                const spyGetRequestParams = spyOn(widgetClass.prototype, "getRequestParams");
                widget = new widgetClass(config);
                expect(spyGetRequestParams).toHaveBeenCalledTimes(1);
            });
            it("constructor calling getPublisherId", () => {
                const spyGetRequestParams = spyOn(widgetClass.prototype, "getPublisherId");
                widget = new widgetClass(config);
                expect(spyGetRequestParams).toHaveBeenCalledTimes(1);
            });
        });
        describe("getRequestParams", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("missing params", () => {
                delete config.params;
                expect(() => {
                    widget = new widgetClass(config);
                    widget.getRequestParams();
                }).toThrowError("Please provide request params");
            });
            it('missing one of the requested params ["app.type", "app.apikey", "source.id"]', () => {
                const missingParam = "app.type";
                delete config.params[missingParam];
                expect(() => {
                    widget = new widgetClass(config);
                    widget.getRequestParams();
                }).toThrowError(`Please provide ${missingParam} parameter`);
            });
            it("initiating params", () => {
                widget = new widgetClass(config);
                expect(widget.getRequestParams()).toEqual(config.params);
            });
        });
        describe("getPublisherID", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("missing publisher id", () => {
                delete config.publisherId;
                expect(() => {
                    widget = new widgetClass(config);
                    widget.getPublisherId();
                }).toThrowError(`Please provide publisher id`);
            });
            it("initiating publisher id", () => {
                widget = new widgetClass(config);
                expect(widget.getPublisherId()).toEqual(config.publisherId);
            });
        });
        describe("getWidgetContainer", () => {
            beforeEach(() => {
                config = JSON.parse(JSON.stringify(testConfig));
            });
            it("container id provided cannot found in page", () => {
                config.widgetContainerId = null;
                widget = new Taboola.UI.Recommendation(config);
                expect(widget.getWidgetContainer()).toEqual(document.body);
            });
            it("container id provided cannot found in page", () => {
                const el = document.createElement("div");
                el.id = "testing-element";
                config.widgetContainerId = el.id;
                document.body.insertBefore(el, document.body.firstChild);
                widget = new Taboola.UI.Recommendation(config);
                expect(widget.getWidgetContainer()).toEqual(el);
                el.remove();
            });
        });
    });
});
