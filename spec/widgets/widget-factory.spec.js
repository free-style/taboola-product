let config = {
    // widget container
    widgetContainerId: "taboola-recommendation-widget-container",
    // params needed for this test
    params: {
        "app.type": "desktop",
        "app.apikey": "f9040ab1b9c802857aa783c469d0e0ff7e7366e4",
        count: "50",
        "source.type": "video",
        "source.id": "214321562187",
        "source.url": "http://www.site.com/videos/214321562187.html",
    },
    publisherId: "taboola-templates",
    widgetType: "recommendation",
};
describe("widget-factory.js", () => {
    describe("widget-factory", () => {
        const widgetFactory = Taboola.UI.widgetFactory;
        const widgetFactoryMap = widgetFactory.widgets;
        describe("constructor", () => {
            it("Taboola.UI.widgetFactory instance exists and instanceof the invoked class", () => {
                expect(typeof widgetFactory).toBe("object");
                expect(widgetFactory instanceof Taboola.UI.widgetFactory.__proto__.constructor).toBeTruthy();
                expect(widgetFactoryMap).toEqual({ recommendation: Taboola.UI.Recommendation });
                expect(typeof widgetFactory.createWidget).toBe("function");
            });
        });
        describe("createWidget", () => {
            it("called with undefined widget type", () => {
                let widgetType = "video-widget";
                expect(() => {
                    widgetFactory.createWidget(widgetType, {});
                }).toThrowError(`${widgetType} is not valid widget type.`);
            });
            it("called with valid widget", () => {
                let widgetType = "recommendation";
                let returnValue = widgetFactory.createWidget(widgetType, config);
                expect(returnValue instanceof widgetFactoryMap[widgetType]).toBeTruthy();
            });
        });
    });
});
