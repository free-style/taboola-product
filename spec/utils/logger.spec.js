describe("logger.js", () => {
    describe("logger", () => {
        const logger = Taboola.utils.logger;
        describe("constructor", () => {
            it("Taboola.utils.logger instance exists and instanceof the invoked class", () => {
                expect(typeof logger).toBe("object");
                expect(logger instanceof Taboola.utils.logger.__proto__.constructor).toBeTruthy();
                expect(typeof logger.log).toBe("function");
                expect(typeof logger.error).toBe("function");
            });
        });
        describe("initializing Taboola.log", () => {});
        describe("initializing Taboola.error", () => {});
        describe("log functionality", () => {});
        describe("error functionality", () => {});
    });
});
