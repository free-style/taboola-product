module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        clientBuildPath: "dist/",
        productBuildPath: "<%=clientBuildPath %>/<%= pkg.name %>",
        productBuildTempPath: "<%=clientBuildPath %>/<%= pkg.name %>/temp",
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
            },
            build: {
                src: "<%=productBuildPath %>/taboola.js",
                dest: "build/<%= pkg.name %>.min.js",
            },
        },
        concat: {
            utils: {
                src: "src/utils/*.js",
                dest: "<%=productBuildTempPath %>/utils.js",
            },
            widgets: {
                src: "src/widgets/**/*.js",
                dest: "<%=productBuildTempPath %>/widgets.js",
            },
            blocks: {
                src: "src/blocks/**/*.js",
                dest: "<%=productBuildTempPath %>/blocks.js",
            },
            jsApi: {
                src: "src/api/*.js",
                dest: "<%=productBuildTempPath %>/api.js",
            },
            taboolaJS: {
                src: "<%=productBuildTempPath %>/*.js",
                dest: "<%=productBuildPath %>/taboola.js",
                options: {
                    stripBanners: true,
                    banner: "/*! <%= pkg.name %> - v<%= pkg.version %> - " + '<%= grunt.template.today("yyyy-mm-dd") %> */',
                    // + grunt.util.linefeed +
                    // "(function(window){" + grunt.util.linefeed + grunt.util.linefeed,
                    // footer:
                    //     "})(window);" +
                    //     grunt.util.linefeed +
                    //     grunt.util.linefeed,
                },
            },
            taboolaCSS: {
                stripBanners: true,
                src: "<%=productBuildTempPath%>/stylesheets/**/*sample-page.css",
                dest: "<%=productBuildPath%>/taboola.css",
            },
        },
        clean: {
            build: ["<%=clientBuildPath %>"],
            buildTemp: ["<%=productBuildTempPath %>"],
            options: {
                force: true,
            },
        },
        copy: {
            samplePageFiles: {
                files: [
                    {
                        cwd: "src/sample-page",
                        src: "*.*",
                        dest: "<%=productBuildPath%>/",
                        expand: true,
                    },
                ],
            },
        },
        sass: {
            options: {
                sourcemap: "none",
                lineNumbers: true,
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "src/",
                        src: ["**/*.scss"],
                        dest: "<%=productBuildTempPath%>/stylesheets/",
                        ext: "sample-page.css",
                    },
                ],
            },
        },
        watch: {
            scripts: {
                files: ["src/**/*.*"],
                tasks: ["default"],
                options: {
                    spawn: true,
                },
            },
        },
        connect: {
            server: {
                options: {
                    livereload: true,
                    port: 9001,
                    base: '<%=clientBuildPath%>'
                }
            }
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks('grunt-contrib-connect');

    // Default task(s).
    grunt.registerTask("default", function () {
        grunt.task.run("clean:build");
        grunt.task.run("copy:samplePageFiles");
        grunt.task.run("sass");
        grunt.task.run("concat");
        grunt.task.run("clean:buildTemp");

        // grunt.task.run("watch");
        // grunt.task.run('uglify');
    });
    grunt.registerTask("watch-dev", function () {
        grunt.task.run("default");
        grunt.task.run("watch");
    });
    grunt.registerTask("serve", function () {
        grunt.task.run("default");
        grunt.task.run("connect");
        grunt.task.run("watch");
    });
};
